<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Event\ImageEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('AppBundle:Image')
            ->findLastOrderedByDate();

        return $this->render('default/index.html.twig', [
            'images' => $images,
        ]);
    }

    /**
     * @Route("image/{imageid}/{basename}", name="image_page", requirements={
     *      "imageid": "\d+"
     * })
     */
    public function imageAction($imageid, $basename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('AppBundle:Image')
            ->find($imageid);

        if (is_null($image)) {
            return $this->redirectToRoute('homepage');
        }

        $comment = new Comment();
        $formComment = $this->createForm('comment', $comment);

        $formComment->handleRequest($request);

        if ($formComment->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $comment->setImage($image);

            if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                $comment->setUser($this->getUser());
            }

            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Comment added, yupi!');

            return $this->redirectToRoute('image_page', [
                'imageid' => $imageid,
                'basename' => $basename,
            ]);
        }

        $liipConfiguration = $this->container->getParameter('liip_imagine.filter_sets');

        $reader = \PHPExif\Reader\Reader::factory(\PHPExif\Reader\Reader::TYPE_NATIVE);
        $imageExif = $reader->read($image->path);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $image->getComments(),
            $request->query->getInt('page', 1), // page number
            5 // limit per page
        );

        return $this->render('default/image.html.twig', [
            'image' => $image,
            'imageCodes' => $liipConfiguration,
            'comments' => $pagination,
            'imageExif' => $imageExif,
            'formComment' => $formComment->createView(),
        ]);
    }

    /**
     * @Route("image/{imageid}/{imageSize}/{filename}", name="image_file", requirements={
     *      "imageid": "\d+"
     * })
     */
    public function imageFileAction($imageid, $imageSize, $filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('AppBundle:Image')
            ->find($imageid);

        $response = new Response();
        $webPath = $this->get('kernel')->getRootDir().'/../web';

        $liipConfiguration = $this->container->getParameter('liip_imagine.filter_sets');
        unset($liipConfiguration['cache']);

        if (
            is_null($image)
            || !in_array($imageSize, array_keys($liipConfiguration))
            || pathinfo($image->path, PATHINFO_BASENAME) != $filename
        ) {
            $imagePath = 'img/photos.png';

            if (in_array($imageSize, array_keys($liipConfiguration))) {
                $imagePath = $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePath, $imageSize);
            } else {
                $imagePath = $webPath.'/'.$imagePath;
            }

            $file = file_get_contents($imagePath);

            //$disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, 'photos.png');
            //$response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', 'image/png');
        } else {
            $imagePath = $this->get('liip_imagine.cache.manager')->getBrowserPath($image->path, $imageSize);

            $event = new ImageEvent($image);
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch('acme.events.sitemap', $event);

            $file = file_get_contents($imagePath);
            $response->headers->set('Content-Type', 'image/png');
        }

        $response->setPublic();
        $response->setMaxAge(63072000);
        $response->setSharedMaxAge(63072000);
        $response->setContent($file);

        return $response;
    }
}
