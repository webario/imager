<?php

namespace AppBundle\Controller;

use AppBundle\Utility\ExifHuman;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ImageController extends Controller
{
    public function imageExifAction(array $exifArray)
    {
        $exifHuman = new ExifHuman($exifArray);

        return $this->render(
            'image/exif.html.twig', [
            'exifHuman' => $exifHuman,
        ]);
    }
}
