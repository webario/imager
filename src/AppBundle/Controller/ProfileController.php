<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{
    public function userPicturesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('AppBundle:Image')
            ->findUserImages($this->getUser());

        return $this->render('profile/user-pictures.html.twig', [
            'images' => $images,
        ]);
    }
}
