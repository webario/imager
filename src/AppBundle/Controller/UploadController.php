<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Form\Type\ImageUploadFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadController extends Controller
{
    /**
     * @Route("/uploading", name="uploading")
     * @Method({"POST"})
     */
    public function formAction(Request $request)
    {
        $file = new Image();
        $formxxx = $this->createForm(new ImageUploadFormType(), $file);

        $formxxx->handleRequest($request);

        if ($formxxx->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                $file->setUser($this->getUser());
            }

            $em->persist($file);
            $em->flush();

            // create a JSON-response with a 200 status code
            return new JsonResponse([
                'files' => [[
                    'thumbnailUrl' => $this->container->get('templating.helper.assets')->getUrl($file->getWebPath()),
                    'url' => $this->container->get('templating.helper.assets')->getUrl($file->getWebPath()),
                    'size' => filesize($file->getWebPath()),
                    'name' => pathinfo($file->getWebPath(), PATHINFO_BASENAME),
                ]],
            ]);
        }

        return $this->render('uploading/form.html.twig', [
            'formxxx' => $formxxx->createView(),
        ]);
    }
}
