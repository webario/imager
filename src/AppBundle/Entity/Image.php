<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Image.
 *
 * @ORM\Table("images")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repositories\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="image")
     */
    protected $comments;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * Image path.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    public $path;

    /**
     * @var string
     *
     * @ORM\Column(name="original_name", type="string", length=255)
     */
    protected $originalName;

    /**
     * @var string
     *
     * @ORM\Column(name="original_ext", type="string", length=4)
     */
    protected $originalExt;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $views = 0;

    /**
     * @var
     *
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $size;

    /**
     * @var
     *
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $width;

    /**
     * @var
     *
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    protected $height;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @Assert\Image(
     *      maxSize="5M",
     *      mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *      maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *      mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     * @Assert\NotBlank
     */
    protected $file;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        $uploadRootPath = __DIR__.'/../../../web/'.$this->getUploadDir();

        if (!is_dir($uploadRootPath)) {
            @mkdir($uploadRootPath, 0777, true);
        }

        return $uploadRootPath;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images/'.date('Y/m');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Image
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Image
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        // The file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // Use the original file name here but you should
        // sanitize it at least to avoid any security issues

        $filename = pathinfo($this->getFile()->getClientOriginalName(), PATHINFO_FILENAME)
            .'-'.hash('sha1', uniqid(mt_rand(), true))
            .'.'.$this->getFile()->guessExtension();

        // Set the path property to the filename where you've saved the file
        $this->path = $this->getUploadDir().'/'.$filename;

        list($width, $height, $type, $attr) = getimagesize($this->getFile()->getRealPath());

        $this->originalName = pathinfo($this->getFile()->getClientOriginalName(), PATHINFO_FILENAME);
        $this->originalExt = $this->getFile()->getClientOriginalExtension();
        $this->size = $this->getFile()->getClientSize();
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->path
        );

        $this->file = null;

//        $authChecker = $this->get('security.authorization_checker');
//        if ($authChecker->isGranted('ROLE_USER')) {
//            $this->user = $this->get('security.token_storage')->getToken()->getUser();
//        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }

        $this->path = null;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set originalName.
     *
     * @param string $originalName
     *
     * @return Image
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * Get originalName.
     *
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set originalExt.
     *
     * @param string $originalExt
     *
     * @return Image
     */
    public function setOriginalExt($originalExt)
    {
        $this->originalExt = $originalExt;

        return $this;
    }

    /**
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * Get originalExt.
     *
     * @return string
     */
    public function getOriginalExt()
    {
        return $this->originalExt;
    }

    /**
     * Set size.
     *
     * @param int $size
     *
     * @return Image
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set width.
     *
     * @param int $width
     *
     * @return Image
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width.
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height.
     *
     * @param int $height
     *
     * @return Image
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height.
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Image
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Image
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add comment.
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Image
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment.
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
