<?php

namespace AppBundle\Entity\Repositories;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class ImageRepository extends EntityRepository
{
    public function findLastOrderedByDate($limit = 6)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT i FROM AppBundle:Image i ORDER BY i.id DESC')
            ->setMaxResults($limit)
            ->getResult();
    }

    public function findUserImages(User $user, $limit = 16)
    {
        return $this->getEntityManager()
            ->createQuery('SELECT i FROM AppBundle:Image i WHERE i.user = :user ORDER BY i.id DESC')
            ->setParameters([
                'user' => $user,
            ])
            ->setMaxResults($limit)
            ->getResult();
    }
}
