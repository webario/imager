<?php

namespace AppBundle\Entity\Repositories;

use Carbon\Carbon;
use AppBundle\Entity\Image;
use AppBundle\Entity\View;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class ViewRepository extends EntityRepository
{
    private $viewPeriod = 120;     // in minutes

    public function saveOpen(Image $image, Request $request)
    {
        $userAgent = $request->headers->get('User-Agent');
        $referer = $request->headers->get('referer');
        $userIp = $request->getClientIp();

        $imageView = new View();
        $imageView->setImage($image);
        $imageView->setUserAgent($userAgent);
        $imageView->setIp($userIp);

        $lastView = $this->createQueryBuilder('v')
            ->select('v')
            ->where('v.image = :image')
            ->andWhere('v.type = :type')
            ->andWhere('v.ip = :ip')
            ->andWhere('v.userAgent = :user_agent')
            ->andWhere('v.created >= :created')
            ->orderBy('v.created', 'DESC')
            ->setParameters([
                'type' => 'hit',
                'image' => $image,
                'ip' => $userIp,
                'user_agent' => $userAgent,
                'created' => Carbon::now()->subMinutes($this->viewPeriod),
            ])
            ->getQuery()
            ->getResult();

        if (empty($lastView)) {
            $image->setViews($image->getViews()+1);

            $imageView->setType('hit');

            $this->getEntityManager()->persist($image);
        }

        $parseUrl = parse_url($referer);

        if (array_key_exists('host', $parseUrl) && $parseUrl['host'] != $request->getHost()) {
            $imageView->setReferer($request->headers->get('referer'));
        }

        $this->getEntityManager()->persist($imageView);
        $this->getEntityManager()->flush();

        return false;
    }
}
