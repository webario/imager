<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    public $comments;

    /**
     * @Assert\Image(
     *      maxSize="1M",
     *      mimeTypes = {"image/jpeg", "image/gif", "image/png"},
     *      maxSizeMessage = "The maxmimum allowed file size is 1MB.",
     *      mimeTypesMessage = "Please upload a valid image."
     * )
     */
    protected $avatarFile;

    // for temporary storage - to delete old image
    private $prevAvatar;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $avatar;

    /**
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user")
     */
    protected $images;

    /**
     * Sets the file used for profile picture uploads.
     *
     * @param UploadedFile $file
     *
     * @return object
     */
    public function setProfilePictureFile(UploadedFile $file = null)
    {
        // set the value of the holder
        $this->avatarFile = $file;
        // check if we have an old image path
        if (isset($this->avatar)) {
            // store the old name to delete after the update
            $this->prevAvatar = $this->avatar;
            $this->avatar = null;
        } else {
            $this->avatar = 'initial';
        }

        return $this;
    }

    /**
     * Get the file used for profile picture uploads.
     *
     * @return UploadedFile
     */
    public function getProfilePictureFile()
    {
        return $this->avatarFile;
    }

    /**
     * Set profilePicturePath.
     *
     * @param string $profilePicturePath
     *
     * @return User
     */
    public function setProfilePicturePath($profilePicturePath)
    {
        $this->avatar = $profilePicturePath;

        return $this;
    }

    /**
     * Get profilePicturePath.
     *
     * @return string
     */
    public function getProfilePicturePath()
    {
        return $this->avatar;
    }

    /**
     * Get the absolute path of the profilePicturePath.
     */
    public function getProfilePictureAbsolutePath()
    {
        return null === $this->avatar
            ? null
            : $this->getUploadRootDir().'/'.$this->avatar;
    }

    /**
     * Get root directory for file uploads.
     *
     * @return string
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        $destDir = __DIR__.'/../../../web/'.$this->getUploadDir();

        if (!is_dir($destDir)) {
            @mkdir($destDir, 0777, true);
        }

        return $destDir;
    }

    /**
     * Specifies where in the /web directory profile pic uploads are stored.
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads/users';
    }

    /**
     * Get the web path for the user.
     *
     * @return string
     */
    public function getAvatarWeb()
    {
        return '/'.$this->getUploadDir().'/'.$this->getProfilePicturePath();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUploadProfilePicture()
    {
        if (null !== $this->getProfilePictureFile()) {
            // a file was uploaded
            // generate a unique filename
            $filename = $this->generateRandomProfilePictureFilename();
            $this->setProfilePicturePath($filename.'.'.$this->getProfilePictureFile()->guessExtension());
        }
    }

    /**
     * Generates a 32 char long random filename.
     *
     * @return string
     */
    public function generateRandomProfilePictureFilename()
    {
        $count = 0;
        do {
            $generator = new SecureRandom();
            $random = $generator->nextBytes(16);
            $randomString = bin2hex($random);
            ++$count;
        } while (file_exists($this->getUploadRootDir().'/'.$randomString.'.'.$this->getProfilePictureFile()->guessExtension()) && $count < 50);

        return $randomString;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     *
     * Upload the profile picture
     *
     * @return mixed
     */
    public function uploadProfilePicture()
    {
        // check there is a profile pic to upload
        if ($this->getProfilePictureFile() === null) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getProfilePictureFile()->move($this->getUploadRootDir(), $this->getProfilePicturePath());

        // check if we have an old image
        if (isset($this->prevAvatar) && file_exists($this->getUploadRootDir().'/'.$this->prevAvatar)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->prevAvatar);
            // clear the temp image path
            $this->prevAvatar = null;
        }
        $this->avatarFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeProfilePictureFile()
    {
        if ($file = $this->getProfilePictureAbsolutePath() && file_exists($this->getProfilePictureAbsolutePath())) {
            unlink($file);
        }
    }

    /**
     * Set avatar.
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar.
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Add image.
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return User
     */
    public function addImage(\AppBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image.
     *
     * @param \AppBundle\Entity\Image $image
     */
    public function removeImage(\AppBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
