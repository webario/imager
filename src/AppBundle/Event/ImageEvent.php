<?php

namespace AppBundle\Event;

use AppBundle\Entity\Image;
use Symfony\Component\EventDispatcher\Event;

class ImageEvent extends Event
{
    private $image;

    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
