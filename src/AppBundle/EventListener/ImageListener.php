<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\View;
use Doctrine\ORM\EntityManager;
use AppBundle\Event\ImageEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class ImageListener
{
    private $em;
    private $requestStack;
    private $request;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function onSitemapEvent(ImageEvent $event)
    {
        $image = $event->getImage();

        $this->em->getRepository('AppBundle:View')
                    ->saveOpen($image, $this->request);
    }
}
