<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CommentFormType extends AbstractType
{
    protected $auth;

    public function __construct(AuthorizationChecker $auth)
    {
        $this->auth = $auth;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$this->auth->isGranted('IS_AUTHENTICATED_FULLY')) {
            $builder->add('username', 'text');
            $builder->add('email', 'text');
        }

        $builder
            ->add('comment', 'textarea')
            ->add('save', 'submit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment',
        ));
    }

    public function getName()
    {
        return 'comment';
    }
}
