<?php

namespace AppBundle\Twig;

use Symfony\Component\Routing\RouterInterface;
use AppBundle\Entity\Image;
use AppBundle\Utility\System;

class AppExtension extends \Twig_Extension
{
    protected $service;

    private $generator;

    public function __construct(RouterInterface $generator)
    {
        $this->generator = $generator;
        $this->service = new System();
    }

    public function getName()
    {
        return 'app_extension';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('filesize', [$this, 'readableFilesize']),
            new \Twig_SimpleFilter('webImageURL', [$this, 'webImageURL']),
        );
    }

    /**
     * Readable file size of files.
     *
     * @param int    $size
     * @param int    $precision
     * @param string $space
     *
     * @return string
     */
    public function readableFilesize($size, $precision = 2, $space = ' ')
    {
        return $this->service->readableFilesize($size, $precision, $space);
    }

    public function webImageURL(Image $image, $size = 'picture_homepage', $withDomain = false)
    {
        if ($image->path === null) {
            return;
        }

        return $this->generator->generate('image_file', [
            'imageid' => $image->id,
            'imageSize' => $size,
            'filename' => pathinfo($image->path, PATHINFO_BASENAME),
        ], $withDomain);
    }
}
