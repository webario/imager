<?php

namespace AppBundle\Utility;

class ExifHuman
{
    public $exifData = [];

    private $system;

    public function __construct(array $exifData)
    {
        $this->exifData = $exifData;
        $this->system = new System();
    }

    public function __call($name, $args)
    {
        if (method_exists($this, $name)) {
            return call_user_func([$this, $name], $args);
        }

        $camelCaseName = strtolower($name);
        if (method_exists($this, 'get'.$camelCaseName)) {
            return call_user_func([$this, 'get'.$camelCaseName], $args);
        }

        return $this->getExifValue($name);
    }

    private function exifClass($label, $value)
    {
        $val = new \stdClass();
        $val->label = $this->system->camelCaseToNormal($label);
        $val->value = $value;

        return $val;
    }

    private function getExifValue($key)
    {
        if (!array_key_exists($key, $this->exifData)) {
            throw new \Exception('Undefined EXIF key - '.$key);
        }

        return $this->exifClass($key, $this->exifData[$key]);
    }

    public function getFileSize()
    {
        $val = $this->system->readableFilesize($this->exifData['FileSize']);

        return $this->exifClass('File size', $val);
    }

    public function getCreationdate()
    {
        return $this->exifClass('Created at', $this->exifData['creationdate']->format('Y-m-d H:i:s'));
    }
}
