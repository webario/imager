<?php

namespace AppBundle\Utility;

class System
{
    /**
     * Readable file size of files.
     *
     * @param int    $size
     * @param int    $precision
     * @param string $space
     *
     * @return string
     */
    public function readableFilesize($size, $precision = 2, $space = ' ')
    {
        if ($size <= 0) {
            return '0'.$space.'KB';
        }
        if ($size === 1) {
            return '1'.$space.'B';
        }
        $mod = 1024;
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];
        for ($i = 0; $size > $mod && $i < count($units) - 1; ++$i) {
            $size /= $mod;
        }

        return round($size, $precision).$space.$units[$i];
    }

    public function camelCaseToNormal($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];

        foreach ($ret as &$match) {
            $match = ($match == strtoupper($match))
                ? strtolower($match)
                : lcfirst($match);
        }

        return ucfirst(implode(' ', $ret));
    }
}
