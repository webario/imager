<?php

namespace TestingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use TestingBundle\Entity\Document;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/hello")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('TestingBundle:Default:index.html.twig');
    }

    /**
     * @Route("/upload", name="testing_upload")
     */
    public function uploadAction(Request $request)
    {
        $document = new Document();

        $form = $this->createFormBuilder($document)
            ->add('name')
            ->add('file')
            ->add('save', 'submit', ['label' => 'Upload'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $document->upload();

            $em->persist($document);
            $em->flush();

            return $this->redirectToRoute('testing_upload');
        }

        return $this->render('TestingBundle:Default:upload.html.twig', [
            'formx' => $form->createView(),
        ]);
    }
}
